equipos = []

n = int(input("Ingrese el numero de equipos: "))
partidos_jugados = int(input("Ingrese el numero de partidos jugados: "))

for i in range(n):
    datos = []
    nombre_equipo = input("Ingrese el nombre del equipo #{}: ".format(i+1))
    datos.append(nombre_equipo)
    datos.append(partidos_jugados)
    partidos_ganados = int(input("Ingrese el numero de partidos ganados para el equipo {}: ".format(nombre_equipo)))
    if (partidos_ganados > partidos_jugados):
        break
    else:
        datos.append(partidos_ganados)
    partidos_perdidos = int(input("Ingrese el numero de partidos perdidos para el equipo {}: ".format(nombre_equipo)))
    if(partidos_ganados+partidos_perdidos > partidos_jugados):
        break
    else:
        datos.append(partidos_perdidos)
    partidos_empatados = int(input("Ingrese el numero de partidos empatados para el equipo {}: ".format(nombre_equipo)))
    if (partidos_ganados+partidos_perdidos+partidos_empatados > partidos_jugados):
        break
    else:
        datos.append(partidos_empatados)
    goles_favor = int(input("Ingrese los goles a favor para el equipo {}: ".format(nombre_equipo)))
    datos.append(goles_favor)
    goles_contra = int(input("Ingrese los goles en contra para el equipo {}: ".format(nombre_equipo)))
    datos.append(goles_contra)
    puntos = partidos_ganados*3+partidos_empatados
    datos.append(puntos)
    diferencia_goles = goles_favor - goles_contra
    if diferencia_goles < 0 :
        diferencia_goles = 0
    datos.append(diferencia_goles)
    equipos.append(datos)

aux = 0
l = 1

for i in range(n):
    for j in range(l,n):
        if equipos[i][7] < equipos[j][7]:
            aux = equipos[i]
            equipos[i] = equipos[j]
            equipos[j] = aux
    l = l+1

aux2 = 0
l2 = 1

for i in range(n):
    for j in range(l2,n):
        if equipos[i][7] == equipos[j][7]:
            if equipos[i][8] < equipos[j][8]:
                aux2 = equipos[i]
                equipos[i] = equipos[j]
                equipos[j] = aux2
    l2 = l2+1

print("")

print("  Nombre del equipo  |       Partidos jugados      |      Partidos ganados     |       Partidos perdidos      | "
      "  Partidos empatados   |       Goles a favor      |       Goles en contra      | "
      "    Puntos    | Diferencia de goles |")

print("")

a = 0

for i in range(a,n):
    print("")
    print("          {}                          {}                           {}                             {}".format(equipos[i][0], equipos[i][1], equipos[i][2], equipos[i][3]),end=' ')
    print("                          {}                          {}                           {}                    {}".format(equipos[i][4], equipos[i][5], equipos[i][6], equipos[i][7]), end=' ')
    print("               {}".format(equipos[i][8]))

